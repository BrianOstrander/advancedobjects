using UnityEngine;
using System.Collections;

namespace AdvancedObjects {

	/// <summary>
	/// A class to provide an easy way of spawning new scripts and prefabs.
	/// </summary>
	public abstract class Spawnable<T> : MonoBehaviour {

		/// <summary>
		/// The location within the Resources folder where spawnable prefabs should be stored.
		/// </summary>
		public const string PrefabPath = "AdvancedObjects/Spawnables";
		
		/// <summary>
		/// Spawns a GameObject with the name of this class, or loads it from the Resources folder if an appropriate prefab is found.
		/// </summary>
		public static Spawnable<T> Spawn() {
			// Attempt to load prefab
			GameObject spawned = (GameObject)Resources.Load(PrefabPath+"/"+typeof(T).Name);
			if (spawned == null) {						
				// if no prefab with the spawnable's name is found, create a GameObject
				spawned = new GameObject();
				spawned.name = typeof(T).Name;
				spawned.AddComponent(typeof(T).Name);
			} else {									
				// if the prefab is found, spawn it
				spawned = (GameObject)Instantiate(spawned);
				spawned.name = typeof(T).Name;
			}
			return (Spawnable<T>)spawned.GetComponent(typeof(T));
		}
	}
}

