using UnityEngine;
using System.Collections;

namespace AdvancedObjects {

	/// <summary>
	/// A class to manage scripts and prefabs that should only be spawned once.
	/// </summary>
	public abstract class SpawnableSingleton<T> : MonoBehaviour where T : MonoBehaviour {

		/// <summary>
		/// The location within the Resources folder where spawnable singleton prefabs should be stored.
		/// </summary>
		public const string PrefabPath = "AdvancedObjects/SpawnableSingletons";
		
		private static T instance;
		
		/// <summary>
		/// Gets the spawned instance of this singleton.
		/// </summary>
		/// <value>Spawned instance of this class, or null if it hasn't been spawned or has been destroyed.</value>
		public static T Instance {
			get {
				if (instance == null || instance.gameObject == null) {
					GameObject existingInstance = GameObject.Find(typeof(T).Name);
					if (existingInstance != null) {
						instance = (T)System.Convert.ChangeType(existingInstance.GetComponent(typeof(T)), typeof(T));
					}
				}
				return instance;
			}
		}

		/// <summary>
		/// Spawns a GameObject with the name of this class, or loads it from the Resources folder if an appropriate prefab is found.
		/// The spawned instance can then be accessed via the Instance parameter.
		/// </summary>
		public static T Spawn() {
			if (Instance == null) {
				GameObject spawned = (GameObject)Resources.Load(PrefabPath+"/"+typeof(T).Name); // Attempt to load prefab
				if (spawned == null) {
					// if no prefab with the spawnable's name is found, create a GameObject
					spawned = new GameObject();
					spawned.name = typeof(T).Name;
					spawned.AddComponent(typeof(T).Name);
				} else {
					// if the prefab is found, spawn it
					spawned = (GameObject)Instantiate(spawned);
					spawned.name = typeof(T).Name;
				}
				instance = (T)System.Convert.ChangeType(spawned.GetComponent(typeof(T)), typeof(T));
				return instance;
			} else {
				Debug.LogWarning("Can't spawn multiple instances of "+typeof(T).ToString()+"!");
				return Instance;
			}
		}
	}
}
