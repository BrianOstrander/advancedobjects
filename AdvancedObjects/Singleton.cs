using UnityEngine;
using System.Collections;

namespace AdvancedObjects {
	
	/// <summary>
	/// A class to manage scripts and prefabs that should only be spawned once.
	/// </summary>
	public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
		
		private static T instance;
		
		/// <summary>
		/// Gets the existing instance of this singleton.
		/// </summary>
		/// <value>Existing instance of this class, or null if it hasn't been spawned or has been destroyed.</value>
		public static T Instance {
			get {
				if (instance == null || instance.gameObject == null) {
					GameObject existingInstance = GameObject.Find(typeof(T).Name);
					if (existingInstance != null) {
						instance = (T)System.Convert.ChangeType(existingInstance.GetComponent(typeof(T)), typeof(T));
					}
				}
				return instance;
			}
		}
	}
}
