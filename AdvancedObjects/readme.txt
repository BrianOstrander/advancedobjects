AdvancedObjects
By Brian Ostrander

This is a plugin for Unity, allowing you to easily create and manage singletons and spawnable objects in your project. Just inherit from them and override the appropriate methods.